#!/usr/bin/env bash

# start services
# supervisord -c '/etc/supervisor/docker.conf' -u root -n >&1
# -n not needed nodaemon=true defined in supervisor section 
exec supervisord -c '/etc/supervisor/docker.conf' >&1
