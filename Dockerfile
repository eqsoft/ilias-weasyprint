FROM ubuntu:bionic

LABEL maintainer="schneider@hrz.uni-marburg.de"

# do not edit
ENV DEBIAN_FRONTEND noninteractive

# os
RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    curl \
    nano \
    supervisor

# weasyprint dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    python3-dev \
    python3-pip \
    python3-cffi python3-setuptools \
    python3-wheel \
    libcairo2 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libgdk-pixbuf2.0-0 \
    libffi-dev \
    shared-mime-info

RUN pip3 install cairocffi weasyprint gunicorn flask

RUN mkdir /myapp && mkdir /ilias-web && mkdir /ilias-data
WORKDIR /myapp

# copy files
COPY services/init/ /usr/local/bin/
COPY services/weasyprint/ /myapp/
COPY services/supervisor/conf.d/ /etc/supervisor/conf.d/
COPY services/supervisor/docker.conf /etc/supervisor/

RUN chmod 755 /usr/local/bin/*

# for documentation
EXPOSE 5001 5002 5003 5004 5005

CMD ["init.sh"]
